mkdir -p build
python3 gen_pack.py midgard.xml > build/midgard_pack.h
python3 bifrost/gen_disasm.py bifrost/ISA.xml > build/bifrost_gen_disasm.c
gcc decode.c bifrost/disassemble.c bifrost/bi_print_common.c build/bifrost_gen_disasm.c -I build/ -I . -I bifrost/ -shared -fPIC -o ~/panloader/libpanfrost_decode.so
